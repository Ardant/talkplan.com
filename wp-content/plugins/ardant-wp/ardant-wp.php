<?php
/*
Plugin Name: Ardant WP
Description: Ardant Wordpress Plugin.
Author: Daniel Smith
Author URI: https://ardant.co.uk
Version: 1.0.0
Plugin URI: https://ardant.co.uk/
Copyright: Daniel Smith
Text Domain: ardant-wp
*/

function class_init() {
	register_taxonomy(
		'classco',
		'post',
		array(
			'label' => __( 'Property Classification' )
		)
	);
}
add_action( 'init', 'class_init' );

add_action( 'init', 'cpt_property' );
function cpt_property() {
	register_post_type( 'property', array(
	  'labels' => array(
	    'name' => 'Properties',
	    'singular_name' => 'Property',
	   ),
	  'description' => 'Custom post type for properties',
	  'public' => true,
	  'menu_position' => 10,
	  'taxonomies' => array('classco'),
	  'supports' => array( 'title','editor')
	));
}













?>