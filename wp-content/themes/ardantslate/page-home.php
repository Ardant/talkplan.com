<?php /* Template name: Homepage */ get_header(); ?>
<section id="content" role="main" class="col-sm-9">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content">

<div class="slick">
  <div>
  TIME LEFT IN YOUR CURRENT CONTRACT? NOT GETTING THE SERVICE THAT YOU REQUIRE? WANT A CHEAPER MONTHLY BILL OR THE NEWEST HANDSET WITHOUT HAVING TO WAIT FOR YOUR EXITING CONTRACT TO EXPIRE? THEN LET US GIVE YOU A CALL – YOU’VE NOTHING TO LOSE AND EVERYTHING TO GAIN. 
  </div>
  <div>
  TALKPLAN.COM BE PART OF SOMETHING BETTER PICTURE (about us) 
  </div>
  <div>
  MOBILE & MOBILE BROADBAND SOLUTIONS
  </div>
  <div>
  OFFICE LANDLINE, VOIP &amp; BROADBAND SOLUTIONS
  </div>
  <div>
  IOT (INTERNET OF THINGS)
  </div>
</div>

<?php the_content(); ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>